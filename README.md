<h1>Get started</h1>

<hr />

<h3>At first:</h3>

1. Install nodejs v13+.<br/>
2. Install gulp cli `npm install --global gulp-cli`.<br/>
3. `npm i`.<br/>

<h5>Dev server:</h5>
`npm start` / `gulp DEV_MODE`

<h4>Don`t edit or delete `./src/index.css`!</h4>

<h5>Production build:</h5>
`npm run build` / `gulp PROD_MODE`

<h5>Test on email:</h5>

1. `npm run build` / `gulp PROD_MODE`<br />
2. Go to file `./sendmail.mjs` and set variables (from, to, pass, subject).
    <p>Variables instruction:</p>
    - <b>from</b> -> message sender email
    - <b>pass</b> -> message sender email passsword
    - <b>to</b> -> recipient email address
    - <b>subject</b> -> subject of message
3. Go to https://www.google.com/settings/security/lesssecureapps and allow less secure apps for email address from variable `from`.
4. `npm run mail`.